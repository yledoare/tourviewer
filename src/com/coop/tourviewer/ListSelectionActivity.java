package com.coop.tourviewer;

import java.util.ArrayList;


import com.coop.tourviewer.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Messenger;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ListSelectionActivity extends Activity{

	private ListView lstRunSelectionList;
	private ArrayList<Tour> tours;
	private ArrayList<POI> pois;
	private ArrayList<Media> media;
	
	private String type = "";
	
	
	public void onCreate(Bundle savedInstanceState) 
    {
    	//Call Super Class
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.selectionlist); 
        
        instantiateControls();
         
        Bundle extras = getIntent().getExtras();
        
        type = (String) extras.get("Type");
        String[] listValues = null; 
        
        
        if(type.compareTo("Tour") == 0)
        {
        	tours = (ArrayList<Tour>) extras.get("Tours");
        	
        	listValues = new String[tours.size()];
        	
        	for(int index = 0; index <= listValues.length - 1; index++)
            {
            	listValues[index] = tours.get(index).getTourName();
            }
        }
        else if (type.compareTo("POI") == 0)
        {
        	pois = (ArrayList<POI>) extras.get("POIs");
        	
        	listValues = new String[pois.size()];
        	
        	for(int index = 0; index <= listValues.length - 1; index++)
            {
            	listValues[index] = pois.get(index).getPoiName();
            }
        }
        else if(type.compareTo("Media") == 0)
        {
        	media = (ArrayList<Media>) extras.get("Media");
        	
        	listValues = new String[media.size()];
        	
        	for(int index = 0; index <= listValues.length - 1; index++)
            {
            	listValues[index] = media.get(index).getMediaName();
            }
        }
        
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
		  android.R.layout.simple_list_item_1, android.R.id.text1, listValues);
	
		
		lstRunSelectionList.setAdapter(adapter); 
	
    
		lstRunSelectionList.setOnItemClickListener(new OnItemClickListener() {
			  public void onItemClick(AdapterView<?> parent, View view,
			    int position, long id) {
				  
				  if(type.compareTo("Tour") == 0)
			      {
					  Toast.makeText(ListSelectionActivity.this, "Atempting to load Tour from the local database",Toast.LENGTH_SHORT).show();
			    	  Intent tourActivity = new Intent(ListSelectionActivity.this, TourActivity.class);
			    	  tourActivity.putExtra("Tour", tours.get(position));  
					  startActivity(tourActivity);	
			      }
			      else if (type.compareTo("POI") == 0)
			      {
			    	  Toast.makeText(ListSelectionActivity.this, "Atempting to load POI from the local database",Toast.LENGTH_SHORT).show();
			    	  Intent poiActivity = new Intent(ListSelectionActivity.this, POIActivity.class);
			    	  poiActivity.putExtra("POI", pois.get(position));  
			    	  startActivity(poiActivity);		
			      }
			      else if(type.compareTo("Media") == 0)
			      {
			    	  Toast.makeText(ListSelectionActivity.this, "Atempting to load Media from the local database",Toast.LENGTH_SHORT).show();
			    	  Intent mediaActivity = new Intent(ListSelectionActivity.this, MediaActivity.class);
			    	  mediaActivity.putExtra("Media", media.get(position));  
			    	  startActivity(mediaActivity);		
			      } 
			  }
			}); 
    
		
    }
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        // use an inflater to populate the ActionBar with items
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.applicationmenu, menu);
        return true;
    }

    //Action bar listener 
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        
        switch(item.getItemId()) 
        {
           case R.id.itemShowMarkerLocation:
           {
        	   if(type.compareTo("Tour") == 0)
               {
        		    Intent intent = new Intent(ListSelectionActivity.this, ViewMapActivity.class);
	        	    
	        	    intent.putExtra("Type", "Tours");
					intent.putExtra("Tours", tours);
			    
					startActivity(intent); 
               }
               else if (type.compareTo("POI") == 0)
               {
            	    Intent intent = new Intent(ListSelectionActivity.this, ViewMapActivity.class);
	        	    
	        	    intent.putExtra("Type", "POIS");
					intent.putExtra("POIS", pois);
			    
					startActivity(intent);
               }
               else if(type.compareTo("Media") == 0)
               {
            	   Toast.makeText(ListSelectionActivity.this, "Nothing to show on map",Toast.LENGTH_SHORT).show();
               }
        	   break;
           }
           
           case R.id.itemShowMyLocation:
           {
        	   Intent intent = new Intent(ListSelectionActivity.this, ViewMapActivity.class);
          	    
        	   Coordinate coordinate = LocationClass.getLocation(this);
        	   
       	       intent.putExtra("Coordinate", coordinate);
       	       intent.putExtra("Type", "Coordinate");
			  
			   startActivity(intent); 
				
        	   break;
           }
           
           case R.id.itemBack:
           {
        	   finish();
        	   break;
           }
        }
        
        return true;
    }
    
	private void instantiateControls()
	{
		lstRunSelectionList = (ListView) findViewById(R.id.lstSelectionList);
	}
}
