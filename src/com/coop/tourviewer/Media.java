package com.coop.tourviewer;

import java.io.Serializable;


public class Media implements Serializable
{

	 //***************************************************************************************************************
    //Attributes
    //***************************************************************************************************************
    private int mediaID;
	private int poiID;
    private int tourID;
    private String mediaName = "";
    private String mediaDescription = "";
    private String mediaType;

    
    //***************************************************************************************************************
    //Constructors
    //***************************************************************************************************************
    public Media()
    {
    	
    }

    public Media (int mid, POI poi, String name, String description, String type)
    {
        this.mediaID = mid;
        this.mediaName = name;
        this.mediaDescription = description;
        this.mediaType = type;
        

        linkToPoi(poi);
    }

    //***************************************************************************************************************
    //Methods
    //***************************************************************************************************************
    public void linkToPoi(POI poi)
    {
        tourID = poi.getTourID();
        poiID = poi.getPoiID();
        poi.addMedia(this);
    }

    //***************************************************************************************************************
    //Getters & Setters
    //***************************************************************************************************************
    public int getMediaID() {
		return mediaID;
	}

	public void setMediaID(int mediaID) {
		this.mediaID = mediaID;
	}

	public int getPoiID() {
		return poiID;
	}

	public void setPoiID(int poiID) {
		this.poiID = poiID;
	}

	public int getTourID() {
		return tourID;
	}

	public void setTourID(int tourID) {
		this.tourID = tourID;
	}

	public String getMediaName() {
		return mediaName;
	}

	public void setMediaName(String mediaName) {
		this.mediaName = mediaName;
	}

	public String getMediaDescription() {
		return mediaDescription;
	}

	public void setMediaDescription(String mediaDescription) {
		this.mediaDescription = mediaDescription;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
}
	
