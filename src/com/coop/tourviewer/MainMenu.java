package com.coop.tourviewer;




import java.util.ArrayList;





import com.coop.tourviewer.R;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class MainMenu extends Activity
{
	 
	//Set up controls
	 private ActionBar actionBar;
	 private Button btnRunTour;
     private Button btnDownloadTour;
     private Button btnExit;
     

    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	//Call Super Class
        super.onCreate(savedInstanceState);
        
        //Set Display to XML Layout
        setContentView(R.layout.mainmenu); 
               
        //Instantiate Controls
        instantiateControls();
        
        btnRunTour.setOnClickListener(new OnClickListener() {
			public void onClick(View v) 
			{
				
				Toast.makeText(MainMenu.this, "Lecture des parcours depuis la base de donnée", Toast.LENGTH_SHORT).show();
				
				Intent intent = new Intent(MainMenu.this, LoadFromDBService.class);
				
				Messenger messenger = new Messenger(dbHandler);
				
				//Pass Handler to Service
				intent.putExtra("MESSENGER", messenger);
				
				//Pass the URL to Service
				intent.putExtra("Type", "Tour");
				
				//Start The Service
				startService(intent);
			}
		});
        
        btnDownloadTour.setOnClickListener(new OnClickListener() {
			public void onClick(View v) 
			{
				Toast.makeText(MainMenu.this, "Téléchargement de la liste des parcours", Toast.LENGTH_SHORT).show();
				
				Intent intent = new Intent(MainMenu.this, DownloadXMLService.class);
				
				Messenger messenger = new Messenger(xmlHandler);
				
				//Pass Handler to Service
				intent.putExtra("MESSENGER", messenger);
				
				//Pass the URL to Service
				Preferences preferences = new Preferences();
				intent.putExtra("URL", preferences.APPLIURL+"xml/test2.xml");
				Log.i("Yann", "Valeur URL = " + preferences.APPLIURL);

				
				//Start The Service
				startService(intent);
			}
		});
        
        
        btnExit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) 
			{
				finish();
			}
		});   
    }

	private void instantiateControls()
	{
		
		//Instantiate Controls
        btnRunTour = (Button) findViewById(R.id.btnRunTour);
        btnDownloadTour = (Button) findViewById(R.id.btnDownloadTour);
        btnExit = (Button) findViewById(R.id.btnExit);
        
        //Setup Action Bar
        actionBar = getActionBar();
	    actionBar.show();    
	    actionBar.setTitle("Main Menu");
	}

		
    	@Override
	    public boolean onCreateOptionsMenu(Menu menu) 
	    {
	        // use an inflater to populate the ActionBar with items
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.applicationmenu, menu);
	        return true;
	    }

	    //Action bar listener 
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item){
	        
	        switch(item.getItemId()) 
	        {
	           case R.id.itemShowMarkerLocation:
	           {
	        	   Toast.makeText(MainMenu.this, "Rien à voir sur la carte", Toast.LENGTH_SHORT).show();
	        	   break;
	           }
	           
	           
	           case R.id.itemShowMyLocation:
	           {
	        	   Intent intent = new Intent(MainMenu.this, ViewMapActivity.class);
	          	    
	        	   Coordinate coordinate = LocationClass.getLocation(this);
	        	   
	       	       intent.putExtra("Coordinate", coordinate);
	       	       intent.putExtra("Type", "Coordinate");
				  
				   startActivity(intent); 
					
	        	   break;
	           }
	           
	           case R.id.itemBack:
	           {
	        	   finish();
	        	   break;
	           }
	        }
	        
	        return true;
	    }
	    
	    
	  //Create handler to handle received messages
		private Handler xmlHandler = new Handler() {
			public void handleMessage(Message message) 
			{	
				//If the file was Streamed and parsed correctly
				if (message.arg1 == RESULT_OK)
				{
					ArrayList<Tour> tours = (ArrayList) message.obj;
					
					String tourCount = String.valueOf(tours.size());
					
					Toast.makeText(MainMenu.this, tourCount + " tours found on web",
							Toast.LENGTH_SHORT).show();
					
					if(tours.size() >= 1)
					{
			    		Intent downloadSelection = new Intent(MainMenu.this, DownLoadSelection.class);
			    		// Intent downloadSelection = new Intent(MainMenu.this, DownloadWithProgressBar.class);
			    		
			    		downloadSelection.putExtra("Tours", tours); 
			    	
						startActivity(downloadSelection);
			    	}
					else
					{
						Toast.makeText(MainMenu.this, "There are no Tours found on the server",Toast.LENGTH_SHORT).show();
					}
				}
				else 
				{
					Toast.makeText(MainMenu.this, "Problème de lecture du fichier XML, merci de vérifier le réseau ",
							Toast.LENGTH_SHORT).show();
				}
			};
		}; 
		
		//Create handler to handle received messages
				private Handler dbHandler = new Handler() {
					public void handleMessage(Message message) 
					{	
						//If the file was Streamed and parsed correctly
						if (message.arg1 == RESULT_OK)
						{
							ArrayList<Tour> tours = (ArrayList) message.obj;
							
							if(tours.size() >= 1)
							{
								String tourCount = String.valueOf(tours.size());
								
								Toast.makeText(MainMenu.this, tourCount + " tours found in the local database",Toast.LENGTH_SHORT).show();
								
								Intent listSelection = new Intent(MainMenu.this, ListSelectionActivity.class);
					    		
					    		listSelection.putExtra("Tours", tours); 
					    		listSelection.putExtra("Type", "Tour");
					    	
								startActivity(listSelection);
							}
							else
							{
								Toast.makeText(MainMenu.this, "There are no Tours in the local database",Toast.LENGTH_SHORT).show();
							}
							
						}
						else 
						{
							Toast.makeText(MainMenu.this, "Failed to load tours from the database",
									Toast.LENGTH_SHORT).show();
						}
					};
				}; 
 
}
