package com.coop.tourviewer;

import java.util.ArrayList;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;

public class WriteToDBService extends IntentService 
{
	

	public WriteToDBService() 
	{
		super("Database Write");
	}
	
	protected void onHandleIntent(Intent intent) 
	{
		
		int result;
		boolean writeOK;
		
		//Get extras from activity
		Bundle extras = intent.getExtras();
		
		Tour tour = (Tour) extras.get("Tour");

                try 
                {
                	DatabaseHandler db = new DatabaseHandler(this);
                	
                	writeOK = db.writeTour(tour);
                	
                	if(writeOK)
                	{
                		result = Activity.RESULT_OK;
                	}
                	else
                	{
                		result = Activity.RESULT_CANCELED;
                	}
                }
                catch (Exception e)
                {
                	result = Activity.RESULT_CANCELED;
                }          
                
                Messenger messenger = (Messenger) extras.get("MESSENGER");        
                Message message = Message.obtain(); 

                message.arg1 = result;
                message.obj = tour;
                
                try
                {
                    messenger.send(message);
                }
                catch (android.os.RemoteException e1)
                {
                	
                }               
            }   		
}
