package com.coop.tourviewer;

import java.io.File;
import java.util.ArrayList;

import com.coop.tourviewer.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class MediaActivity extends Activity{

	//Set up controls
		 private ActionBar actionBar;
		 private TextView txtMediaID;
		 private TextView txtMediaPOIID;
	     private TextView txtMediaTourID;
	     private TextView txtMediaName;
	     private TextView txtMediaDescription;
	     
	     private Button   btnViewMediaItem;
	    
	     private Media media = null;
	     
	    @Override
	    public void onCreate(Bundle savedInstanceState) 
	    {
	    	//Call Super Class
	        super.onCreate(savedInstanceState);
	        
	        //Set Display to XML Layout
	        setContentView(R.layout.mediaactivity); 
	               
	        
	        Bundle extras = getIntent().getExtras();
	        
	        media = (Media) extras.get("Media");
	        
	        //Instantiate Controls
	        instantiateControls();
	        
	        populateActivity();
	        
	        btnViewMediaItem.setOnClickListener(new OnClickListener() {
				public void onClick(View v) 
				{
					Toast.makeText(MediaActivity.this, "Loading Media for Preview", Toast.LENGTH_SHORT).show();
					
					Intent intent = new Intent(MediaActivity.this, MediaPreview.class);
					intent.putExtra("Media", media);
			    
					startActivity(intent);
				}
			});
	    }
	    
	        
	    private void instantiateControls()
		{
			
	    	txtMediaID = (TextView) findViewById(R.id.txtMediaID);
			txtMediaPOIID	= (TextView) findViewById(R.id.txtMediaPOIID);
			txtMediaTourID = (TextView) findViewById(R.id.txtMediaTourID);
			txtMediaName = (TextView) findViewById(R.id.txtMediaName);
			txtMediaDescription = (TextView) findViewById(R.id.txtMediaDescription);
			btnViewMediaItem = (Button)  findViewById(R.id.btnViewMediaItem);
				
	        //Setup Action Bar
	        actionBar = getActionBar();
	       
		    actionBar.show();    
	       
		    actionBar.setTitle("Media View");
		}
	    
	    private void populateActivity()
	    {
	    	txtMediaID.setText(String.valueOf(media.getMediaID()));
	    	txtMediaPOIID.setText(String.valueOf(media.getPoiID()));
			txtMediaTourID.setText(String.valueOf(media.getTourID()));
			txtMediaName.setText(media.getMediaName() + media.getMediaType());
			txtMediaDescription.setText(media.getMediaDescription());
			
	    }
	    
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) 
	    {
	        // use an inflater to populate the ActionBar with items
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.applicationmenu, menu);
	        return true;
	    }

	    //Action bar listener 
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item){
	    	switch(item.getItemId()) 
	        {
	           case R.id.itemShowMarkerLocation:
	           {
	        	   Toast.makeText(MediaActivity.this, "Nothing to show on Map", Toast.LENGTH_SHORT).show();
	        	   break;
	           }
	           
	           
	           case R.id.itemShowMyLocation:
	           {
	        	   Intent intent = new Intent(MediaActivity.this, ViewMapActivity.class);
	          	    
	        	   Coordinate coordinate = LocationClass.getLocation(this);
	        	   
	       	       intent.putExtra("Coordinate", coordinate);
	       	       intent.putExtra("Type", "Coordinate");
				  
				   startActivity(intent); 
					
	        	   break;
	           }
	           
	           case R.id.itemBack:
	           {
	        	   finish();
	        	   break;
	           }
	        }
	         
	        return true;
	    }
}
		    

