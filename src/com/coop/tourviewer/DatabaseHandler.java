package com.coop.tourviewer;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHandler extends SQLiteOpenHelper {
	 
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 19;
 
    // Database Name
    private static final String DATABASE_NAME = "tourProgramDatabase3";
 
  //********************************************************************************
  //TOUR TABLE
  //********************************************************************************
    private static final String TABLE_TOURS = "tours";
    
    private static final String TOUR_TOUR_ID = "tourid";
    private static final String TOUR_CREATOR_ID = "creatorid";
    private static final String TOUR_TOUR_NAME = "tourname";
    private static final String TOUR_TOUR_ABOUT = "tourabout";
    private static final String TOUR_LATITUDE = "latitude";
    private static final String TOUR_LONGITUDE = "longitude"; 
  //********************************************************************************
    
    
  //********************************************************************************
  //POI TABLE
  //********************************************************************************
    private static final String TABLE_POIS = "pois";
  
    private static final String POI_POI_ID = "poiid";
    private static final String POI_TOUR_ID = "tourid";
    private static final String POI_POI_NAME = "poiname";
    private static final String POI_POI_DESCRIPTION = "poidescription";
    private static final String POI_POI_ORDER = "poiorder";
    private static final String POI_LATITUDE = "latitude";
    private static final String POI_LONGITUDE = "longitude";
  //********************************************************************************
    
    
  //********************************************************************************
  //MEDIA TABLE
  //********************************************************************************
    private static final String TABLE_MEDIAS = "medias";
    
    private static final String MEDIA_MEDIA_ID = "mediaid";
    private static final String MEDIA_POI_ID = "poiid";
    private static final String MEDIA_TOUR_ID = "tourid";
    private static final String MEDIA_MEDIA_NAME = "medianame";
    private static final String MEDIA_MEDIA_DESCRIPTION = "mediadescription";
    private static final String MEDIA_MEDIA_TYPE = "mediatype";
  //********************************************************************************
    
    
   
 
    public DatabaseHandler(Context context) 
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) 
    {
        String CREATE_TOUR_TABLE = "CREATE TABLE " + TABLE_TOURS + "(" +
               TOUR_TOUR_ID + " INTEGER PRIMARY KEY," + 
        	   TOUR_CREATOR_ID + " INTEGER," +
        	   TOUR_TOUR_NAME + " TEXT," +
        	   TOUR_TOUR_ABOUT + " TEXT," +
        	   TOUR_LATITUDE + " REAL," +
               TOUR_LONGITUDE + " REAL" + ")";
        
        String CREATE_POI_TABLE = "CREATE TABLE " + TABLE_POIS + "(" +
        	     POI_POI_ID + " INTEGER PRIMARY KEY," +
                 POI_TOUR_ID + " INTEGER," + 
         	     POI_POI_NAME + " TEXT," +
         	     POI_POI_DESCRIPTION + " TEXT," +
         	     POI_POI_ORDER + " INTEGER," +         	   
         	     POI_LATITUDE + " REAL," +
                 POI_LONGITUDE + " REAL" + ")";
        
        String CREATE_MEDIA_TABLE = "CREATE TABLE " + TABLE_MEDIAS + "(" +
        	   MEDIA_MEDIA_ID + " INTEGER PRIMARY KEY," +
        	   MEDIA_POI_ID + " INTEGER," +
        	   MEDIA_TOUR_ID + " INTEGER," +
        	   MEDIA_MEDIA_NAME + " TEXT," + 
        	   MEDIA_MEDIA_DESCRIPTION + " TEXT," +
        	   MEDIA_MEDIA_TYPE + " TEXT" + ")";
        
        
        db.execSQL(CREATE_TOUR_TABLE);
        db.execSQL(CREATE_POI_TABLE);
        db.execSQL(CREATE_MEDIA_TABLE);
    }
    
 
    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOURS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_POIS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEDIAS);
 
        // Create tables again
        onCreate(db);
    }
    
    public ArrayList<Tour> getTours()
    {    	
    	ArrayList<Tour> tours = new ArrayList<Tour>();
    	
    	String selectQuery = "SELECT * FROM " + TABLE_TOURS;

    	SQLiteDatabase db = this.getWritableDatabase();

    	Cursor cursor = db.rawQuery(selectQuery, null);
    	
    	 if (cursor.moveToFirst())
    	 {
   	        do {
		    	 Tour tour = new Tour();
		         
		         tour.setTourID(Integer.parseInt(cursor.getString(0)));
		         tour.setCreatorID(Integer.parseInt(cursor.getString(1)));
		         tour.setTourName(cursor.getString(2));
		         tour.setTourAbout(cursor.getString(3));
		         tour.getStartCoordinate().setLatitude(Double.parseDouble(cursor.getString(4)));
		         tour.getStartCoordinate().setLongitude(Double.parseDouble(cursor.getString(5)));
		        
		         tours.add(tour);
   	        } while (cursor.moveToNext());
    	 }
    	 
    	 return tours;
    }
    
    public Tour getTour(int tourID)
    {
    	Tour tour = null;
    	
    	try
    	{
	    	SQLiteDatabase db = this.getReadableDatabase();
	   	 
	        Cursor cursor = db.query(TABLE_TOURS, new String[] { TOUR_TOUR_ID,
	                TOUR_CREATOR_ID, TOUR_TOUR_NAME, TOUR_TOUR_ABOUT, TOUR_LATITUDE,
	                TOUR_LONGITUDE}, TOUR_TOUR_ID + "=?",
	                new String[] { String.valueOf(tourID) }, null, null, null, null);
	        
	        
	        if (cursor != null)
	            cursor.moveToFirst();
	     
	        tour = new Tour();
	        
	        tour.setTourID(Integer.parseInt(cursor.getString(0)));
	        tour.setCreatorID(Integer.parseInt(cursor.getString(1)));
	        tour.setTourName(cursor.getString(2));
	        tour.setTourAbout(cursor.getString(3));
	        tour.getStartCoordinate().setLatitude(Double.parseDouble(cursor.getString(4)));
	        tour.getStartCoordinate().setLongitude(Double.parseDouble(cursor.getString(5)));
	        tour.setHasMedia(Boolean.getBoolean(cursor.getString(6)));
    	}
    	catch(Exception e)
    	{
    	
    	}
    	
        return tour;
    }
    
    @SuppressLint("NewApi")
	public ArrayList<POI> getPOI(Tour tour)
    {
    	 
	    ArrayList<POI> poiList = new ArrayList<POI>();
	    
	    // Select All Query
	    String selectQuery = "SELECT  * FROM " + TABLE_POIS + " WHERE " +
	    					  POI_TOUR_ID + " =? ORDER BY " + POI_POI_ORDER;
	    
	    String[] parameters = { String.valueOf(tour.getTourID()) };
	 
	    SQLiteDatabase db = this.getWritableDatabase();
	    
	    Cursor cursor = db.rawQuery(selectQuery, parameters);
	    
	    //Cursor cursor = db.rawQuery(selectQuery,null);
	    
	    // looping through all rows and adding to list
	    if (cursor.moveToFirst()) {
	        do {
	            POI poi = new POI();
	            
	            poi.setPoiID(Integer.parseInt(cursor.getString(0)));
	            poi.setTourID(Integer.parseInt(cursor.getString(1)));
	            poi.setPoiName(cursor.getString(2));
	            poi.setPoiDescription(cursor.getString(3));
	            poi.setPoiOrder(Integer.parseInt(cursor.getString(4)));
	            poi.getGpsCoords().setLatitude(Double.parseDouble(cursor.getString(5)));
	            poi.getGpsCoords().setLongitude(Double.parseDouble(cursor.getString(6)));
	            
	            
	            // Adding contact to list
	            poiList.add(poi);
	            
	        } while (cursor.moveToNext());
	    }
	 
	    // return contact list
	    return poiList;
    }
    
    public ArrayList<Media> getMedia(POI poi)
    {
    	 ArrayList<Media> mediaList = new ArrayList<Media>();
 	    
 	    // Select All Query
 	    String selectQuery = "SELECT  * FROM " + TABLE_MEDIAS + " WHERE " +
 	    					  MEDIA_POI_ID + " =?";
 	    
 	    String[] parameters = { String.valueOf(poi.getPoiID()) };
 	 
 	    SQLiteDatabase db = this.getWritableDatabase();
 	    
 	    Cursor cursor = db.rawQuery(selectQuery, parameters);
 	 
 	    // looping through all rows and adding to list
 	    if (cursor.moveToFirst()) {
 	        do {
 	            Media media = new Media();
 	            
 	            media.setMediaID(Integer.parseInt(cursor.getString(0)));
 	            media.setPoiID(Integer.parseInt(cursor.getString(1)));
 	            media.setTourID(Integer.parseInt(cursor.getString(2)));	
 	            media.setMediaName(cursor.getString(3));
 	            media.setMediaDescription(cursor.getString(4));
 	            media.setMediaType(cursor.getString(5));
 	           
 	            // Adding contact to list
 	            mediaList.add(media);
 	            
 	        } while (cursor.moveToNext());
 	    }
 	 
 	    // return contact list
 	    return mediaList;
    }  
    
    public Boolean writeTour(Tour tour)
    {
    	try
    	{
		    SQLiteDatabase db = this.getWritableDatabase();
		 
		    ContentValues values = new ContentValues();
		    
		    values.put(TOUR_TOUR_ID, tour.getTourID());
		    values.put(TOUR_CREATOR_ID, tour.getCreatorID());
		    values.put(TOUR_TOUR_NAME, tour.getTourName());
		    values.put(TOUR_TOUR_ABOUT, tour.getTourAbout());
		    values.put(TOUR_LATITUDE, tour.getStartCoordinate().getLatitude());
		    values.put(TOUR_LONGITUDE, tour.getStartCoordinate().getLongitude());

		    
		    // Inserting Row
		    db.insert(TABLE_TOURS, null, values);
		    db.close(); // Closing database connection
		    
		    
		    for (int index = 0; index <= tour.getPoiList().size() - 1; index ++)
		    {
		    	writePOI(tour.getPoiList().get(index));
		    	
		    	for (int index2 = 0; index2 <= tour.getPoiList().get(index).getPoiMedia().size() - 1; index2++)
		    	{
		    		writeMedia(tour.getPoiList().get(index).getPoiMedia().get(index2));
		    	}
		    }
		    
		    return true;
    	}
    	catch(Exception e)
    	{
    		return false;
    	}
    }
    
    private void writePOI(POI poi)
    {
	    SQLiteDatabase db = this.getWritableDatabase();
	 
	    ContentValues values = new ContentValues();
	    
	    values.put(POI_POI_ID, poi.getPoiID());
	    values.put(POI_TOUR_ID, poi.getTourID());
	    values.put(POI_POI_NAME, poi.getPoiName());
	    values.put(POI_POI_DESCRIPTION, poi.getPoiDescription());
	    values.put(POI_POI_ORDER, poi.getPoiOrder());
	    values.put(POI_LATITUDE, poi.getGpsCoords().getLatitude());
	    values.put(POI_LONGITUDE, poi.getGpsCoords().getLongitude());
	    
	    // Inserting Row
	    db.insert(TABLE_POIS, null, values);
	    db.close(); // Closing database connection
    }
    
    private void writeMedia(Media media)
    {
	    SQLiteDatabase db = this.getWritableDatabase();
	 
	    ContentValues values = new ContentValues();
	    
	    values.put(MEDIA_MEDIA_ID, media.getMediaID());
	    values.put(MEDIA_POI_ID, media.getPoiID());
	    values.put(MEDIA_TOUR_ID, media.getTourID());
	    values.put(MEDIA_MEDIA_NAME, media.getMediaName());
	    values.put(MEDIA_MEDIA_DESCRIPTION, media.getMediaDescription());
	    values.put(MEDIA_MEDIA_TYPE, media.getMediaType());
	    
	    // Inserting Row
	    db.insert(TABLE_MEDIAS, null, values);
	    db.close(); // Closing database connection
    }
    
    public void deleteTour(Tour tour)
    {
    	
    }
}