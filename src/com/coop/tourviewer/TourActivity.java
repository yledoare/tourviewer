package com.coop.tourviewer;

import java.util.ArrayList;

import com.coop.tourviewer.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TourActivity extends Activity{

	//Set up controls
		 private ActionBar actionBar;
		 private Button btnViewPOIs;
	     private TextView txtTourID;
	     private TextView txtCreatorID;
	     private TextView txtTourName;
	     private TextView txtTourAbout;
	     private TextView txtLatitude;
	     private TextView txtLongitude;
	     
	     private Tour tour = null;

	    @Override
	    public void onCreate(Bundle savedInstanceState) 
	    {
	    	//Call Super Class
	        super.onCreate(savedInstanceState);
	        
	        //Set Display to XML Layout
	        setContentView(R.layout.touractivity); 
	               
	        
	        Bundle extras = getIntent().getExtras();
	        
	        tour = (Tour) extras.get("Tour");
	        
	        //Instantiate Controls
	        instantiateControls();
	        
	        populateActivity();
	        
	        
	        btnViewPOIs.setOnClickListener(new OnClickListener() {
				public void onClick(View v) 
				{
					Toast.makeText(TourActivity.this, "Getting POIs from local database", Toast.LENGTH_SHORT).show();
					
					Intent intent = new Intent(TourActivity.this, LoadFromDBService.class);
					
					Messenger messenger = new Messenger(dbHandler);
					
					//Pass Handler to Service
					intent.putExtra("MESSENGER", messenger);
					
					//Pass the URL to Service
					intent.putExtra("Type", "POI");
					
					intent.putExtra("Tour", tour);
					
					//Start The Service
					startService(intent);
				}
			});
	    }
	        
	    private void instantiateControls()
		{
			btnViewPOIs = (Button) findViewById(R.id.btnViewPOIs);
			txtTourID = (TextView) findViewById(R.id.txtTourID);
			txtCreatorID = (TextView) findViewById(R.id.txtCreatorID);
			txtTourName = (TextView) findViewById(R.id.txtTourName);
			txtTourAbout = (TextView) findViewById(R.id.txtTourAbout);
			txtLatitude = (TextView) findViewById(R.id.txtLatitude);
	        txtLongitude = (TextView) findViewById(R.id.txtLongitude);
	        
	        //Setup Action Bar
	        actionBar = getActionBar();
	       
		    actionBar.show();    
	       
		    actionBar.setTitle("Tour View");
		}
	    
	    private void populateActivity()
	    {
			txtTourID.setText(String.valueOf(tour.getTourID()));
			txtCreatorID.setText(String.valueOf(tour.getCreatorID()));
			txtTourName.setText(tour.getTourName());
			txtTourAbout.setText(tour.getTourAbout());
			txtLatitude.setText(String.valueOf(tour.getStartCoordinate().getLatitude()));
	        txtLongitude.setText(String.valueOf(tour.getStartCoordinate().getLongitude()));
	        
	    }

	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) 
	    {
	        // use an inflater to populate the ActionBar with items
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.applicationmenu, menu);
	        return true;
	    }

	    //Action bar listener 
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item){
	        
	        switch(item.getItemId()) 
	        {
	           case R.id.itemShowMarkerLocation:
	           {
	        	    Intent intent = new Intent(TourActivity.this, ViewMapActivity.class);
	        	    
	        	    intent.putExtra("Type", "Tour");
					intent.putExtra("Tour", tour);
			    
					startActivity(intent);
	        	   
	        	   break;
	           }
	        
	           case R.id.itemShowMyLocation:
	           {
	        	   Intent intent = new Intent(TourActivity.this, ViewMapActivity.class);
	          	    
	        	   Coordinate coordinate = LocationClass.getLocation(this);
	        	   
	       	       intent.putExtra("Coordinate", coordinate);
	       	       intent.putExtra("Type", "Coordinate");
				  
				   startActivity(intent); 
					
	        	   break;
	           }
	           
	           case R.id.itemBack:
	           {
	        	   finish();
	        	   break;
	           }
	        }
	        
	        return true;
	    }
		    
		    
			//Create handler to handle received messages
					private Handler dbHandler = new Handler() {
						public void handleMessage(Message message) 
						{	
							//If the file was Streamed and parsed correctly
							if (message.arg1 == RESULT_OK)
							{
								ArrayList<POI> pois = (ArrayList) message.obj;
								
								if(pois.size() >= 1)
								{
									String poiCount = String.valueOf(pois.size());
									
									Toast.makeText(TourActivity.this, poiCount + " POIs found in local database",
											Toast.LENGTH_SHORT).show();
									
									Intent listSelection = new Intent(TourActivity.this, ListSelectionActivity.class);
						    		
						    		listSelection.putExtra("POIs", pois);
						    		listSelection.putExtra("Type", "POI");
						    	
									startActivity(listSelection);
								}
								else
								{
									Toast.makeText(TourActivity.this, "There are no POIs for this Tour in the local database",Toast.LENGTH_SHORT).show();
								}
								
							}
							else 
							{
								Toast.makeText(TourActivity.this, "Failed to load POIs from the database",Toast.LENGTH_SHORT).show();
							}
						};
					}; 
	    }
		    

