package com.coop.tourviewer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.os.Messenger;
import android.widget.Toast;

public class UnzipFileService extends IntentService{

	public UnzipFileService() {
		super("UnzipService");
	}

	@Override
	protected void onHandleIntent(Intent intent) 
	{
		
		int result;
		
		Tour tour = null;
		
		//Get extras from activity
		Bundle extras = intent.getExtras();
		
		tour = (Tour) extras.get("Tour");

               
                	File sdDir = Environment.getExternalStorageDirectory();
                	
                	boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
                	
                
                	String path = sdDir + "/media/";
                	
                    String inputFile = path + tour.getTourID() + ".zip";
                    
                    InputStream is;
                    ZipInputStream zis;
                    
                    try 
                    {
                        String filename;
                        is = new FileInputStream(inputFile);
                        zis = new ZipInputStream(new BufferedInputStream(is));          
                        ZipEntry ze;
                        byte[] buffer = new byte[4096];
                        int count;

                        while ((ze = zis.getNextEntry()) != null) 
                        {
                            
                            filename = ze.getName(); 
                            FileOutputStream fout = new FileOutputStream(path + filename);

                            while ((count = zis.read(buffer)) != -1) 
                            {
                                fout.write(buffer, 0, count);             
                            }

                            fout.close();               
                            zis.closeEntry();
                        }

                        zis.close();
                    
                        result = Activity.RESULT_OK;
                      } 
                      catch(Exception e)
                      { 
                    	  result = Activity.RESULT_CANCELED;
                      } 
                    
                  //Create a Messenger Object and assign the Messenger from the calling Activity
                    Messenger messenger = (Messenger) extras.get("MESSENGER");
                    
                    //Assign an empty message to message object
                    Message message = Message.obtain(); 

                    //Assign result and parsedXMLObject to message
                    message.arg1 = result;
                    
                    //Attempt to send message
                    try
                    {
                    	//Send message
                        messenger.send(message);
                    }
                    //Catch exceptions that may occur during the execution of this remote method call
                    catch (android.os.RemoteException e1)
                    {
                    	
                    }               
                   
                  
	}
}
