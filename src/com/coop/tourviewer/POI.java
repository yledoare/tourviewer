package com.coop.tourviewer;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class POI implements Serializable
{
	 private int     poiID;
     private int     tourID;
     private String  poiName;
     private String  poiDescription;
     private int     poiOrder;
     private Coordinate gpsCoords = new Coordinate();

     private List<Media> poiMedia = new ArrayList<Media>();

     public POI()
     {

     }

     //TO-DO Switch Coordinate && order
      public POI (int pid, Tour tour, String name, String description, Coordinate coordinate, int order)
     {
         this.poiID          = pid;
         this.poiName        = name;
         this.poiDescription = description;
         this.gpsCoords      = coordinate;
         this.poiOrder       = order;
         linkToTour(tour);
     }

       
      public void linkToTour(Tour linkTour)
      {
          tourID = linkTour.getTourID();
          linkTour.addPOI(this);
      }

      public void addMedia(Media addToMedia)
      {
          this.poiMedia.add(addToMedia);
      }

     public void removeMedia(Media removeMedia)
     {
         this.poiMedia.remove(removeMedia);
     }

     public void setPOIMedia (ArrayList<Media> value)
     {
   	  poiMedia = new ArrayList<Media>();

         for(int index = 0; index <= value.size() - 1; index++)
         {
        	 value.get(index).linkToPoi(this);
         }
   	  
     }
     
     public List<Media> getPoiMedia() {
 		return poiMedia;
 	}

	public int getPoiID() {
		return poiID;
	}

	public void setPoiID(int poiID) {
		this.poiID = poiID;
	}

	public int getTourID() {
		return tourID;
	}

	public void setTourID(int tourID) {
		this.tourID = tourID;
	}

	public String getPoiName() {
		return poiName;
	}

	public void setPoiName(String poiName) {
		this.poiName = poiName;
	}

	public String getPoiDescription() {
		return poiDescription;
	}

	public void setPoiDescription(String poiDescription) {
		this.poiDescription = poiDescription;
	}

	public int getPoiOrder() {
		return poiOrder;
	}

	public void setPoiOrder(int poiOrder) {
		this.poiOrder = poiOrder;
	}

	public Coordinate getGpsCoords() {
		return gpsCoords;
	}

	public void setGpsCoords(Coordinate gpsCoords) {
		this.gpsCoords = gpsCoords;
	}	
}
