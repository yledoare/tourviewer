package com.coop.tourviewer;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.widget.Toast;

public class LocationClass {

	public LocationClass() {
		// TODO Auto-generated constructor stub
	}
	
	public static Coordinate getLocation(Context context)
	{
		    Location gpslocation = getLocationByProvider(LocationManager.GPS_PROVIDER, context);
		    Location networkLocation = getLocationByProvider(LocationManager.NETWORK_PROVIDER, context);

		    Coordinate coordinate = new Coordinate();
		    
		    if(gpslocation == null && networkLocation == null)
		    {
		    	Toast.makeText(context, "Location services not available", Toast.LENGTH_SHORT).show();
		    	
		    	return null;
		    }
		    else if(gpslocation != null)
		    {
		    	Toast.makeText(context, "GPS location found", Toast.LENGTH_SHORT).show();
		    	coordinate.setLatitude(gpslocation.getLatitude());
		    	coordinate.setLongitude(gpslocation.getLongitude());
		        return null;
		    }
		    else if (networkLocation != null)
		    {
		    	Toast.makeText(context, "Network location found", Toast.LENGTH_SHORT).show();
		    	coordinate.setLatitude(networkLocation.getLatitude());
		    	coordinate.setLongitude(networkLocation.getLongitude());
		    	
		    	return coordinate;
		    }
		 
			return null;
		    
	}

private static Location getLocationByProvider(String provider, Context context) 
{
    Location location = null;
   
    LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

    try 
    {
        if (locationManager.isProviderEnabled(provider)) 
        {

            location = locationManager.getLastKnownLocation(provider);
        }
        
    } catch (IllegalArgumentException e) {
    	
    }
    
    return location;
}
}
