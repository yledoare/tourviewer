package com.coop.tourviewer;

import java.util.ArrayList;

import com.coop.tourviewer.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class DownLoadSelection extends Activity
{
	private ListView lstDownloadList;
	private ArrayList<Tour> tours;
	
	public void onCreate(Bundle savedInstanceState) 
    {
    	//Call Super Class
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.downloadselection); 
        
        instantiateControls();
         
        Bundle extras = getIntent().getExtras();
        
        tours = (ArrayList<Tour>) extras.get("Tours");
        
        String[] listValues = new String[tours.size()];
        
        for(int index = 0; index <= listValues.length - 1; index++)
        {
        	listValues[index] = tours.get(index).getTourName();
        }
        
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
		  android.R.layout.simple_list_item_1, android.R.id.text1, listValues);
	
		
		lstDownloadList.setAdapter(adapter); 
	
    
		lstDownloadList.setOnItemClickListener(new OnItemClickListener() {
			  public void onItemClick(AdapterView<?> parent, View view,
			    int position, long id) {
				  
				  Toast.makeText(DownLoadSelection.this, "Atempting to write Tour to the local database",Toast.LENGTH_SHORT).show();
				  
				  Intent writeTourToDB = new Intent(DownLoadSelection.this, WriteToDBService.class);
		    		
				  Messenger messenger = new Messenger(writeHandler);
					
					writeTourToDB.putExtra("MESSENGER", messenger);
					
		    		writeTourToDB.putExtra("Tour", tours.get(position)); 
		    	
					startService(writeTourToDB); 	
			  }
			}); 
    }
	
	private void instantiateControls()
	{
		lstDownloadList = (ListView) findViewById(R.id.lstDownloadedTours);
	}
	
	private Handler writeHandler = new Handler() {
		public void handleMessage(Message message) 
		{	
			//If the file was Streamed and parsed correctly
			if (message.arg1 == RESULT_OK)
			{
				Tour tour = (Tour) message.obj;
				
				Toast.makeText(DownLoadSelection.this, "Tour has been written to local database",Toast.LENGTH_SHORT).show();
				
				Toast.makeText(DownLoadSelection.this, "Atempting to download Tour media to the SD card",Toast.LENGTH_SHORT).show();
				
				// YLD Intent fileDownload = new Intent(DownLoadSelection.this, FileDownloadService.class);
				
				Intent fileDownload = new Intent(DownLoadSelection.this, DownloadWithProgressBar.class);
				
				Log.i("Yann", "Lancement DownloadWithProgressBar ");

				Messenger messenger = new Messenger(downloadHandler);
				
				fileDownload.putExtra("MESSENGER", messenger);
				Bundle b = new Bundle();
	    		fileDownload.putExtra("Tour", tour);
	    		
	    		startActivity(fileDownload);
	    		Log.i("Yann", "Après startService(fileDownload) ");
				
			}
			else 
			{
				Toast.makeText(DownLoadSelection.this, "A database error has occurred, Tour was not written",
						Toast.LENGTH_SHORT).show();
			}
		};
	}; 
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        // use an inflater to populate the ActionBar with items
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.applicationmenu, menu);
        return true;
    }

    //Action bar listener 
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        
        switch(item.getItemId()) 
        {
           case R.id.itemShowMarkerLocation:
           {
        	   Intent intent = new Intent(DownLoadSelection.this, ViewMapActivity.class);
       	    
       	       intent.putExtra("Type", "Tours");
			   intent.putExtra("Tours", tours);
		    
				startActivity(intent); 
				
        	   break;
           }
           
           
           case R.id.itemShowMyLocation:
           {
        	   Intent intent = new Intent(DownLoadSelection.this, ViewMapActivity.class);
          	    
        	   Coordinate coordinate = LocationClass.getLocation(this);
        	   
       	       intent.putExtra("Coordinate", coordinate);
       	       intent.putExtra("Type", "Coordinate");
			  
			   startActivity(intent); 
				
        	   break;
           }
           
           case R.id.itemBack:
           {
        	   finish();
        	   break;
           }
        }
        
        return true;
    }
	private Handler downloadHandler = new Handler() {
		public void handleMessage(Message message) 
		{	
			if (message.arg1 == RESULT_OK)
			{
				Toast.makeText(DownLoadSelection.this, "Tour media has been downloaded",Toast.LENGTH_SHORT).show();
				
				Tour tour = (Tour) message.obj;
				
				Toast.makeText(DownLoadSelection.this, "Atempting to unzip Tour media to the SD card",Toast.LENGTH_SHORT).show();
				
				Intent fileUnzip = new Intent(DownLoadSelection.this, UnzipFileService.class);
				
				Messenger messenger = new Messenger(unZipHandler);
				
				fileUnzip.putExtra("MESSENGER", messenger);
				
	    		fileUnzip.putExtra("Tour", tour);
	    		
	    		startService(fileUnzip);
			}
			else 
			{
				Toast.makeText(DownLoadSelection.this, "An error occurred downloading the Tour media",Toast.LENGTH_SHORT).show();
			}
		};
	}; 
	
	private Handler unZipHandler = new Handler() {
		public void handleMessage(Message message) 
		{	
			if (message.arg1 == RESULT_OK)
			{ 
				Toast.makeText(DownLoadSelection.this, "Tour media has been unziped",Toast.LENGTH_SHORT).show();
			}
			else 
			{
				Toast.makeText(DownLoadSelection.this, "An error occurred unzipping the Tour media",Toast.LENGTH_SHORT).show();
			}
		};
	}; 

}
