package com.coop.tourviewer;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.coop.tourviewer.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

@SuppressLint("ParserError")
public class ViewMapActivity extends MapActivity
{
	Tour tour = null;
	ArrayList<Tour> tours = null;
	
	POI poi = null;
	ArrayList<POI> pois = null;
	
	Coordinate coordinate = null;
	
	String type = null;
	
	ActionBar actionBar = null;
	

	@Override
	protected boolean isRouteDisplayed() 
	{
	    return false;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.mapview);
	    
	  //Setup Action Bar
        actionBar = getActionBar();
	    actionBar.show();    
	    actionBar.setTitle("Main Menu");
	    
	    Bundle extras = getIntent().getExtras();
        
	    type = extras.getString("Type");
	    
	    MapView mapView = (MapView) findViewById(R.id.mapview);
	    
	    mapView.setBuiltInZoomControls(true);
	    
	    List<Overlay> mapOverlays = mapView.getOverlays();
	    Drawable drawable = this.getResources().getDrawable(R.drawable.marker);
	    MapOverlayClass itemizedoverlay = new MapOverlayClass(drawable, this);
	    
	    MapController mapController = mapView.getController();
	    
	    
	    if(type.compareTo("Tour") == 0)
	    {
	    	tour = (Tour) extras.getSerializable("Tour");
	    	GeoPoint position = new GeoPoint((int)(tour.getStartCoordinate().getLatitude() * 1E6), 
	    								     (int)(tour.getStartCoordinate().getLongitude() * 1E6));
	    	
	    	OverlayItem overlayitem = new OverlayItem(position, String.valueOf(tour.getTourID()),tour.getTourName());
	    
	    	itemizedoverlay.addOverlay(overlayitem);
	    	
	    	mapController.setCenter(position);
	    	mapController.setZoom(14);
	    	
	    	mapOverlays.add(itemizedoverlay);
	    }
	    else if(type.compareTo("Tours") == 0)
	    {
		    	tours = (ArrayList<Tour>) extras.getSerializable("Tours");
		    	
		    	GeoPoint position = null;
		    	
		    	for(int index = 0; index <= tours.size() -1; index++)
		    	{
			    	position = new GeoPoint((int)(tours.get(index).getStartCoordinate().getLatitude() * 1E6), 
			    								     (int)(tours.get(index).getStartCoordinate().getLongitude() * 1E6));
			    	
			    	OverlayItem overlayitem = new OverlayItem(position, String.valueOf(tours.get(index).getTourID()), tours.get(index).getTourName());
			    
			    	
			    	itemizedoverlay.addOverlay(overlayitem);
		    	}
		    	
		    	mapController.setZoom(3);
		    	
		    	mapOverlays.add(itemizedoverlay);
	    }
	    else if(type.compareTo("POI") == 0)
	    {
	    	poi = (POI) extras.getSerializable("POI");
	    	GeoPoint position = new GeoPoint((int)(poi.getGpsCoords().getLatitude() * 1E6), 
	    								     (int)(poi.getGpsCoords().getLongitude() * 1E6));
	    	
	    	OverlayItem overlayitem = new OverlayItem(position, String.valueOf(poi.getPoiID()),poi.getPoiName());
	    
	    	itemizedoverlay.addOverlay(overlayitem);
	    	
	    	mapController.setCenter(position);
	    	mapController.setZoom(14);
	    	
	    	mapOverlays.add(itemizedoverlay);
	    }
	    else if(type.compareTo("POIS") == 0)
	    {
	    	pois = (ArrayList<POI>) extras.getSerializable("POIS");
	    	
	    	GeoPoint position = null;
	    	
	    	for(int index = 0; index <= pois.size() -1; index++)
	    	{
		    	position = new GeoPoint((int)(pois.get(index).getGpsCoords().getLatitude() * 1E6), 
		    								     (int)(pois.get(index).getGpsCoords().getLongitude() * 1E6));
		    	
		    	OverlayItem overlayitem = new OverlayItem(position, String.valueOf(pois.get(index).getPoiID()),pois.get(index).getPoiName());
		    
		    	itemizedoverlay.addOverlay(overlayitem);
	    	}
	    	
	    	if(position != null)
	    	{
	    		mapController.setCenter(position);
	    		mapController.setZoom(14);
	    	}
	    	
	    	mapOverlays.add(itemizedoverlay);
	    }
	    else if(type.compareTo("Coordinate") == 0)
	    {
	    	coordinate = (Coordinate) extras.getSerializable("Coordinate");
	    	
	    	if(coordinate != null)
	    	{
		    	GeoPoint position = new GeoPoint((int)(coordinate.getLatitude() * 1E6), 
		    								     (int)(coordinate.getLongitude() * 1E6));
		    	
		    	OverlayItem overlayitem = new OverlayItem(position,"Your Location","");
		    
		    	itemizedoverlay.addOverlay(overlayitem);
		    	
		    	mapController.setCenter(position);
	    		mapController.setZoom(14);
	    		
		    	mapOverlays.add(itemizedoverlay);	
	    	}
	    }
	    
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        // use an inflater to populate the ActionBar with items
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.applicationmenu, menu);
        return true;
    }

    //Action bar listener 
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        
        switch(item.getItemId()) 
        {
           case R.id.itemShowMarkerLocation:
           {
        	   Toast.makeText(ViewMapActivity.this, "Your already on the map", Toast.LENGTH_SHORT).show();
        	   break;
           }
           
           case R.id.itemShowMyLocation:
           {
        	   if(type.compareTo("Coordinate") == 0)
        	   {
        		   Toast.makeText(ViewMapActivity.this, "Your current location is currently being shown", Toast.LENGTH_SHORT).show();
        	   }
        	   else
        	   {
	        	   Intent intent = new Intent(ViewMapActivity.this, ViewMapActivity.class);
	          	    
	        	   Coordinate coordinate = LocationClass.getLocation(this);
	        	   
	       	       intent.putExtra("Coordinate", coordinate);
	       	       intent.putExtra("Type", "Coordinate");
				  
				   startActivity(intent); 
        	   }
        	   break;
           }
           
           case R.id.itemBack:
           {
        	   finish();
        	   break;
           }
        }
        
        return true;
    }
}
