package com.coop.tourviewer;

import java.util.ArrayList;

import com.coop.tourviewer.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class POIActivity extends Activity{

	//Set up controls
		 private ActionBar actionBar;
		 private Button btnViewMedia;
		 private TextView txtPOIID;
	     private TextView txtPOITourID;
	     private TextView txtPOIName;
	     private TextView txtPOIDescription;
	     private TextView txtPOIOrder;
	     private TextView txtPOILatitude;
	     private TextView txtPOILongitude;
	     
	     private POI poi = null;

	    @Override
	    public void onCreate(Bundle savedInstanceState) 
	    {
	    	//Call Super Class
	        super.onCreate(savedInstanceState);
	        
	        //Set Display to XML Layout
	        setContentView(R.layout.poiactivity); 
	               
	        
	        Bundle extras = getIntent().getExtras();
	        
	        poi = (POI) extras.get("POI");
	        
	        //Instantiate Controls
	        instantiateControls();
	        
	        populateActivity();
	        
	        
	        btnViewMedia.setOnClickListener(new OnClickListener() {
				public void onClick(View v) 
				{
					Toast.makeText(POIActivity.this, "Getting Media from local database", Toast.LENGTH_SHORT).show();
					
					Intent intent = new Intent(POIActivity.this, LoadFromDBService.class);
					
					Messenger messenger = new Messenger(dbHandler);
					
					//Pass Handler to Service
					intent.putExtra("MESSENGER", messenger);
					
					//Pass the URL to Service
					intent.putExtra("Type", "Media");
					
					intent.putExtra("POI", poi);
					
					//Start The Service
					startService(intent);
				}
			});
	    }
	        
	    private void instantiateControls()
		{
			btnViewMedia = (Button) findViewById(R.id.btnViewMedia);
			txtPOIID	= (TextView) findViewById(R.id.txtPOIID);
			txtPOITourID = (TextView) findViewById(R.id.txtPOITourID);
			txtPOIName = (TextView) findViewById(R.id.txtPOIName);
			txtPOIDescription = (TextView) findViewById(R.id.txtPOIDescription);
			txtPOIOrder = (TextView) findViewById(R.id.txtPOIOrder);
			txtPOILatitude = (TextView) findViewById(R.id.txtPOILatitude);
	        txtPOILongitude = (TextView) findViewById(R.id.txtPOILongitude);
	        
	        //Setup Action Bar
	        actionBar = getActionBar();
	       
		    actionBar.show();    
	       
		    actionBar.setTitle("POI View");
		}
	    
	    private void populateActivity()
	    {
	    	txtPOIID.setText(String.valueOf(poi.getPoiID()));
			txtPOITourID.setText(String.valueOf(poi.getTourID()));
			txtPOIName.setText(poi.getPoiName());
			txtPOIDescription.setText(poi.getPoiDescription());
			txtPOIOrder.setText(String.valueOf(poi.getPoiOrder()));
			txtPOILatitude.setText(String.valueOf(poi.getGpsCoords().getLatitude()));
	        txtPOILongitude.setText(String.valueOf(poi.getGpsCoords().getLongitude()));
	    }

	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) 
	    {
	        // use an inflater to populate the ActionBar with items
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.applicationmenu, menu);
	        return true;
	    }

	    //Action bar listener 
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item){
	        
	    	switch(item.getItemId()) 
	        {
	           case R.id.itemShowMarkerLocation:
	           {
	        	    Intent intent = new Intent(POIActivity.this, ViewMapActivity.class);
	        	    
	        	    intent.putExtra("Type", "POI");
					intent.putExtra("POI", poi);
			    
					startActivity(intent);
	        	   
	        	   break;
	           }
	        
	           case R.id.itemShowMyLocation:
	           {
	        	   Intent intent = new Intent(POIActivity.this, ViewMapActivity.class);
	          	    
	        	   Coordinate coordinate = LocationClass.getLocation(this);
	        	   
	       	       intent.putExtra("Coordinate", coordinate);
	       	       intent.putExtra("Type", "Coordinate");
				  
				   startActivity(intent); 
					
	        	   break;
	           }
	           
	           case R.id.itemBack:
	           {
	        	   finish();
	        	   break;
	           }
	        }
	        
	        return true;
	    }
		    
		    
			//Create handler to handle received messages
					private Handler dbHandler = new Handler() {
						public void handleMessage(Message message) 
						{	
							//If the file was Streamed and parsed correctly
							if (message.arg1 == RESULT_OK)
							{
								ArrayList<Media> media = (ArrayList) message.obj;
								
								if(media.size() >= 1)
								{
									Intent listSelection = new Intent(POIActivity.this, ListSelectionActivity.class);
						    		
									listSelection.putExtra("Type", "Media");
						    		listSelection.putExtra("Media", media); 
						    	
									startActivity(listSelection);
								}
								else
								{
									Toast.makeText(POIActivity.this, "There is no Media for this POI in the local database",Toast.LENGTH_SHORT).show();
								}
								
							}
							else 
							{
								Toast.makeText(POIActivity.this, "Failed to load Media from the database",Toast.LENGTH_SHORT).show();
							}
						};
					}; 
	    }
		    

