package com.coop.tourviewer;



import java.io.File;

import com.coop.tourviewer.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class Splash extends Activity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        
    	//Call super class
    	super.onCreate(savedInstanceState);
        
    	//Set view to splash screen
        setContentView(R.layout.splash);
        
        //Instantiate ImageView
        ImageView imgSplash = (ImageView) findViewById(R.id.imgSplash);
        
        boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        
        if(isSDPresent)
        {
        	File directory = new File(Environment.getExternalStorageDirectory()+File.separator+"media");
        	directory.mkdirs();
        }
        else
        {
        	Toast.makeText(this, "No SD Card is Present, media will be disabled",Toast.LENGTH_LONG).show();
        }
        
        
        //Set onClick listener for splash screen
        imgSplash.setOnClickListener(new OnClickListener() {
			public void onClick(View v) 
			{
				 //Displays Main Menu
		      		//Creates New Intent To Load Main Menu
		      		Intent mainmenu = new Intent(Splash.this, MainMenu.class);
				
		      		//Starts Main Menu
		      		startActivityForResult(mainmenu,0);
			}
		}); 
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) 
    {
    	this.finish();
    }
       
}

