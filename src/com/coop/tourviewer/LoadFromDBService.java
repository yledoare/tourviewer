package com.coop.tourviewer;

import java.net.URL;
import java.util.ArrayList;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;

public class LoadFromDBService extends IntentService
{
	public LoadFromDBService() {
		super("Database Read");
	}
	
	protected void onHandleIntent(Intent intent) 
	{
		
		int result;
		String type;
		ArrayList<Tour> tours = new ArrayList<Tour>();
		ArrayList<POI>  pois = new ArrayList<POI>();
		ArrayList<Media> medias = new ArrayList<Media>();
		
		Tour tour = null;
		POI  poi = null;
		
		//Get extras from activity
		Bundle extras = intent.getExtras();
		
		type = extras.getString("Type");
		
		
	
        if (type.compareTo("POI") == 0)
        {
        	tour = (Tour) extras.get("Tour");
        }
        else if(type.compareTo("Media") == 0)
        {
        	poi = (POI) extras.get("POI");
        }

                try 
                {
                	DatabaseHandler db = new DatabaseHandler(this);
                	
                	if(type.compareTo("Tour") == 0)
                	{
                		tours = db.getTours();
                	}
                	else if (type.compareTo("POI") == 0)
                	{
                		pois = db.getPOI(tour);
                	}
                	else if (type.compareTo("Media") == 0)
                	{
                		medias = db.getMedia(poi);
                	}
                	
                	result = Activity.RESULT_OK;
                }
                catch (Exception e)
                {
                	result = Activity.RESULT_CANCELED;
                }          
                
               
                Messenger messenger = (Messenger) extras.get("MESSENGER");        
                Message message = Message.obtain(); 

                message.arg1 = result;
                
                if(type.compareTo("Tour") == 0)
            	{
                	message.obj = tours;
            	}
            	else if (type.compareTo("POI") == 0)
            	{
            		message.obj = pois;
            	}
            	else if (type.compareTo("Media") == 0)
            	{
            		message.obj = medias;
            	}
                
                try
                {
                    messenger.send(message);
                }
                catch (android.os.RemoteException e1)
                {
                	
                }               
            }   		
	}
	
