package com.coop.tourviewer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import com.coop.tourviewer.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class MediaPreview extends Activity
{

	private Media media = null;
	
	

	 private TextView txtView;
     private VideoView vidView;
     private ImageView imgView;
     private File	   mediaFile;
     private ActionBar actionBar;
     
     private MediaController mediaController = null;
	
	
	public void onCreate(Bundle savedInstanceState) 
    {
    	//Call Super Class
        super.onCreate(savedInstanceState);
        
        
        Bundle extras = getIntent().getExtras();
        
        media = (Media) extras.get("Media");
        
        String filepath = Environment.getExternalStorageDirectory() + "/media/" + (media.getMediaName() + media.getMediaType());
        
        mediaFile = new File(filepath);
        
        
        
        if(!mediaFile.exists())
        {
        	Toast.makeText(MediaPreview.this, "This media file is not on the SD card, please wait for media to finish downloading.", Toast.LENGTH_SHORT).show();
        	finish();
        }
        else
        {
        	Toast.makeText(MediaPreview.this, "This media file was found on the SD card.", Toast.LENGTH_SHORT).show();
        	
        	if(media.getMediaType().compareTo(".jpg") == 0)
        	{ 
        		displayImage();
        	}
        	else if (media.getMediaType().compareTo(".txt") == 0)
        	{
        		displayText();
        	}
        	else if (media.getMediaType().compareTo(".mp4") == 0 || media.getMediaType().compareTo(".mp3") == 0)
        	{
        		displayAudioVideo();
        	}
        	
        	actionBar = getActionBar();
 	       
		    actionBar.show();    
	       
		    actionBar.setTitle("Media Preview");
        }
    }
	
	private void displayImage()
	{
        setContentView(R.layout.imagepreview); 
        
        try
        {
        	Bitmap picture=BitmapFactory.decodeFile(mediaFile.getPath());
	        int width = picture.getWidth();
	        int height = picture.getWidth();
	        float aspectRatio = (float) width / (float) height;
	        int newWidth = 1000;
	        int newHeight = (int) (1000 / aspectRatio);       
	        picture= Bitmap.createScaledBitmap(picture, newWidth, newHeight, true);
	        imgView = (ImageView) findViewById(R.id.imgView);
	        imgView.setImageBitmap(picture);
        }
        catch(Exception e)
        {
        	Toast.makeText(MediaPreview.this, "There was an error displaying this media file, please redownload the tour from the main menu", Toast.LENGTH_SHORT).show();
        	finish();
        }
	}
	
	private void displayAudioVideo()
	{
		setContentView(R.layout.videopreview); 
		
		try
		{
			vidView  = (VideoView) findViewById(R.id.vidView);		
	        mediaController = new MediaController(this);	        
	        vidView.setMediaController(mediaController);
	        vidView.setVideoPath(mediaFile.getPath());
	        vidView.requestFocus();
	        vidView.start();   
		}
		catch (Exception e)
		{
			Toast.makeText(MediaPreview.this, "There was an error displaying this media file, please redownload the tour from the main menu", Toast.LENGTH_SHORT).show();
        	finish();
		}
	}

	private void displayText()
	{
		setContentView(R.layout.textpreview);
		
		StringBuilder text = new StringBuilder();

		try 
		{
		    BufferedReader br = new BufferedReader(new FileReader(mediaFile.getPath()));
		    String line;

		    while ((line = br.readLine()) != null)
		    {
		        text.append(line);
		        text.append('\n');
		    }
		    
		  
			txtView = (TextView)findViewById(R.id.txtView);

			//Set the text
			txtView.setText(text);
		}
		catch (Exception e) 
		{
		    
		}

	}
	
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        // use an inflater to populate the ActionBar with items
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.applicationmenu, menu);
        return true;
    }

    //Action bar listener 
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        
        switch(item.getItemId()) 
        {
           case R.id.itemShowMarkerLocation:
           {
        	   Toast.makeText(MediaPreview.this, "There is nothing to show on the map", Toast.LENGTH_SHORT).show();
        	   
        	   break;
           }
        
           case R.id.itemShowMyLocation:
           {
        	   Intent intent = new Intent(MediaPreview.this, ViewMapActivity.class);
          	    
        	   Coordinate coordinate = LocationClass.getLocation(this);
        	   
       	       intent.putExtra("Coordinate", coordinate);
       	       intent.putExtra("Type", "Coordinate");
			  
			   startActivity(intent); 
				
        	   break;
           }
           
           case R.id.itemBack:
           {
        	   finish();
        	   break;
           }
        }
        
        return true;
    }
	
	
	
}
