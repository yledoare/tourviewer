package com.coop.tourviewer;

import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.widget.TextView;

public class DownloadXMLService extends IntentService {

	public DownloadXMLService() {
		super("DownloadService");
	}

	@Override
	protected void onHandleIntent(Intent intent) 
	{
		
		int result;
		
		ArrayList<Tour> tours;
		
		
		//Get extras from activity
		Bundle extras = intent.getExtras();
		

                try {
                        //Create a url object using the location parameter
                        URL url = new URL(extras.getString("URL"));
                       // android.os.Debug.waitForDebugger();
                        //Create the SAXParserFactory Object
                        SAXParserFactory saxFactory = SAXParserFactory.newInstance();
                        
                        //Using the SAXParserFactory create a new SAXParser
                        SAXParser saxParser = saxFactory.newSAXParser();
 
                        //Using the SAXParser create a new XMLReader
                        XMLReader xmlReader = saxParser.getXMLReader();
                         
                        //Create custom handler object
                        CustomHandler myCustomHandler = new CustomHandler();
                        
                        //Set xmlReader's content handler to custom handler
                        xmlReader.setContentHandler(myCustomHandler);
                       
                        //Parse the XML File using custom handler
                        xmlReader.parse(new InputSource(url.openStream()));
                        
                        
                     
                        
                        //Request parsed data in object form
                         tours = myCustomHandler.getTours();
                        
                        //Set result to OK (-1)
                        result = Activity.RESULT_OK;
                }
                catch (Exception e)
                {
                	//Set Result to Canceled (0)
                	result = Activity.RESULT_CANCELED;
                	//  android.os.Debug.waitForDebugger();
                	
                	//Set Object to null
                	tours = null;
                }          
                
                //Create a Messenger Object and assign the Messenger from the calling Activity
                Messenger messenger = (Messenger) extras.get("MESSENGER");
                
                //Assign an empty message to message object
                Message message = Message.obtain(); 

                //Assign result and parsedXMLObject to message
                message.arg1 = result;
                message.obj = tours;
                
                //Attempt to send message
                try
                {
                	//Send message
                    messenger.send(message);
                }
                //Catch exceptions that may occur during the execution of this remote method call
                catch (android.os.RemoteException e1)
                {
                	
                }               
            }   
        }
		
	




