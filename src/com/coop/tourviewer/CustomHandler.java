package com.coop.tourviewer;
 
import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class CustomHandler extends DefaultHandler{
	
	private enum ElementName
	{
	    Tours, Tour, TourID, CreatorID, TourName, TourAbout, Coordinate,
	    POI, POIID, POIName, POIDescription, POIOrder, Laditude, Longitude,
	    Media, MediaID, MediaName, MediaDescription, MediaType, NOVALUE;
	    
	    //Catch IllegalArgumentException or NullPointerException
	    public static ElementName toElement(String elementName)
	    {
	        try {
	            return valueOf(elementName);
	        } 
	        catch (Exception ex) {
	        	//  android.os.Debug.waitForDebugger();
	            return NOVALUE;
	        }
	    }   
	}
     
	//StringBuffer used for characters method
	private StringBuffer stringBuffer = new StringBuffer(); 
	 
	//Used for tag checking
	private boolean inTours, inTour, inTourID, inCreatorID, inTourName, inTourAbout, inCoordinate,
					inPOI, inPOIID, inPOIName, inPOIDescription, inPOIOrder, inLaditude, inLongitude,
					inMedia, inMediaID, inMediaName, inMediaDescription, inMediaType, inTourTag, inPOITag, inMediaTag; 
	 
	//Object used to hold the parsed data
	private ArrayList<Tour> tours = new ArrayList<Tour>();
	private ArrayList<POI> pois = new ArrayList<POI>();
	private ArrayList<Media> medias = new ArrayList<Media>();
	
	
	
	private Tour tempTour;
	private POI  tempPOI;
	private Media tempMedia;
	 
	
	  public CustomHandler()
	  {
			
	  }
	  
	  public ArrayList<Tour> getTours() 
	  { 
	    return tours; 
	  } 
	   
	  @Override 
	  //Called when the document is opened
	  public void startDocument() throws SAXException 
	  { 
	    tours = new ArrayList<Tour>(); 
	  } 
	 
	  @Override 
	  //Called when the document is closed
	  public void endDocument() throws SAXException
	  { 
	 
	  } 
		  
	  @Override 
	  //This is called every time a element is hit, Sets tag booleans
	  public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException 
	  { 
		
		//Clear Buffer
		stringBuffer.delete(0, stringBuffer.length());
		
		//android.os.Debug.waitForDebugger();
		/*inTours, inTour, inTourID, inCreatorID, inTourName, inTourAbout, inCoordinate,
		inPOI, inPOIID, inPOIName, inPOIDescription, inPOIOrder, inLaditude, inLongitude,
		inMedia, inMediaName, inMediaDescription, inMediaType;*/ 
		
		switch (ElementName.toElement(localName))
		{
		case Tours             :inTours = true; break;
		
		case Tour	           :inTour = true;
								inTourTag = true;
								inPOITag = false;
								inMediaTag = false;
					            tempTour = new Tour(); break;
					  
		case TourID	           :inTourID = true; break;
		case CreatorID         :inCreatorID = true; break;
		case TourName          :inTourName = true; break;
		case TourAbout         :inTourAbout = true; break;
		case Coordinate        :inCoordinate = true; break;
		
		case POI               :inPOI = true; 
								inPOITag = true;
								inTourTag = false;
							    inMediaTag = false;
						        tempPOI = new POI(); break;
						        
		case POIID             :inPOIID = true;
						 
		case POIName           :inPOIName = true; break;
		case POIDescription    :inPOIDescription = true; break;
		case POIOrder	       :inPOIOrder = true; break;
		case Laditude	       :inLaditude = true; break;
		case Longitude         :inLongitude = true; break;
		
		case Media             :inMedia  = true;
								inMediaTag = true;
								inTourTag = false;
								inPOITag = false;
		                        tempMedia = new Media(); break;
		                        
		case MediaID           :inMediaID = true; break;
		case MediaName         :inMediaName = true; break;
		case MediaDescription  :inMediaDescription = true; break;
		case MediaType         :inMediaType = true; break;
		
		default 	 :
					inTour = false;
					inTourTag = false; 
					inTourID = false;
					inCreatorID = false;
					inTourName = false;
					inTourAbout = false;
					inCoordinate = false;
					inPOI = false;
					inPOITag = false;
					inPOIName = false;
					inPOIDescription = false;
					inPOIOrder = false;
					inLaditude = false;
					inLongitude = false;
					inMedia  = false;
					inMediaTag = false;
					inMediaName = false;
					inMediaDescription = false;
					inMediaType = false;
	   break;
	   }
	  }
		  
	  //Called when end element is reached
	  @Override 
	  public void endElement(String namespaceURI, String localName, String qName) throws SAXException
	  {   
		  
			//android.os.Debug.waitForDebugger();
			
		  switch (ElementName.toElement(localName))
			{
			case Tours             :inTours = false; break;
			
			case Tour	           :inTour = false;
									tempTour.setPOIList(pois);
								    tours.add(tempTour); 
								    pois = new ArrayList<POI>();break;
									
			case TourID	           :inTourID = false;
									if(inTourTag)
										{tempTour.setTourID(Integer.parseInt(stringBuffer.toString()));break;}
									if(inPOITag)
										{tempPOI.setTourID(Integer.parseInt(stringBuffer.toString()));break;}
									if(inMediaTag)
										{tempMedia.setTourID(Integer.parseInt(stringBuffer.toString()));break;}
										
			case CreatorID         :inCreatorID = false; 
									tempTour.setCreatorID(Integer.parseInt(stringBuffer.toString()));break;
			
			case TourName          :inTourName = false; 
									tempTour.setTourName(stringBuffer.toString());break;
									
			case TourAbout         :inTourAbout = false; 
									tempTour.setTourAbout(stringBuffer.toString());break;
									
			case Coordinate        :inCoordinate = false; break;
			
			
			case POI               :inPOI = false; 
									tempPOI.setPOIMedia(medias);
							        pois.add(tempPOI);
							        medias = new ArrayList<Media>();break;
							        
			case POIID			   :inPOIID = false;
									if(inPOITag)
										{tempPOI.setPoiID(Integer.parseInt(stringBuffer.toString()));break;}
									if(inMediaTag)
										{tempMedia.setPoiID(Integer.parseInt(stringBuffer.toString()));break;}
																 
			case POIName           :inPOIName = false; 
									tempPOI.setPoiName(stringBuffer.toString());break;
									
			case POIDescription    :inPOIDescription = false; 
									tempPOI.setPoiDescription(stringBuffer.toString());break;
									
			case POIOrder	       :inPOIOrder = false; 
									tempPOI.setPoiOrder(Integer.parseInt(stringBuffer.toString()));break;
									
			case Laditude	       :inLaditude = false; 
									if(inTourTag)
										{tempTour.getStartCoordinate().setLatitude(Double.parseDouble(stringBuffer.toString()));break;}
									if(inPOITag)
										{tempPOI.getGpsCoords().setLatitude(Double.parseDouble(stringBuffer.toString()));break;}
			
			case Longitude         :inLongitude = false;
									if(inTourTag)
										{tempTour.getStartCoordinate().setLongitude(Double.parseDouble(stringBuffer.toString()));break;}
									if(inPOITag)
										{tempPOI.getGpsCoords().setLongitude(Double.parseDouble(stringBuffer.toString()));break;}
		
			case Media             :inMedia  = false;
			                        medias.add(tempMedia); break;
			                        
			case MediaID           :inMediaID = false; 
									tempMedia.setMediaID(Integer.parseInt(stringBuffer.toString()));break;
			
			case MediaName         :inMediaName = false; 
									tempMedia.setMediaName(stringBuffer.toString());break;
									
			case MediaDescription  :inMediaDescription = false; 
									tempMedia.setMediaDescription(stringBuffer.toString());break;
									
			case MediaType         :inMediaType = false; 
									tempMedia.setMediaType(stringBuffer.toString());break;
			
			default 	 :
						inTour = false;
						inTourTag = false; 
						inTourID = false;
						inCreatorID = false;
						inTourName = false;
						inTourAbout = false;
						inCoordinate = false;
						inPOI = false;
						inPOITag = false;
						inPOIName = false;
						inPOIDescription = false;
						inPOIOrder = false;
						inLaditude = false;
						inLongitude = false;
						inMedia  = false;
						inMediaTag = false;
						inMediaName = false;
						inMediaDescription = false;
						inMediaType = false;
		   break;
		   }
	  } 
	 

	  @Override 
	  public void characters(char ch[], int start, int length) 
	  {
		 stringBuffer.append(ch, start, length); 
	  } 
	} 

    

