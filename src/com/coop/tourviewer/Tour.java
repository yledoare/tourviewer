package com.coop.tourviewer;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Tour implements Serializable
{
    private int tourID;
    private int creatorID;
    private String tourName;
    private String tourAbout;
    private Coordinate startCoordinate = new Coordinate();
    private List<POI> poiList = new ArrayList<POI>();
    private boolean hasMedia = false;

    public Tour()
    {
        

    }

    public Tour (int tid, int creatorID, String name, String about, Coordinate start)
    {
        this.tourID = tid;
        this.creatorID = creatorID;
        this.tourName = name;
        this.tourAbout = about;
        this.startCoordinate = start;
    }
   
    public void addPOI (POI poi)
    {
        this.poiList.add(poi);
    }

    public void removePOI(POI poiToRemove)
    {
        this.poiList.remove(poiToRemove);
    }
  
    public void setPOIList (List<POI> value)
    {
    	this.poiList = new ArrayList<POI>();
     
    	for(int index = 0; index <= value.size() -1; index++)
    	{
    		value.get(index).linkToTour(this); 		
    	}
    }
    
    public List<POI> getPoiList() {
		return poiList;
	}

	public int getTourID() {
		return tourID;
	}

	public void setTourID(int tourID) {
		this.tourID = tourID;
	}

	public int getCreatorID() {
		return creatorID;
	}

	public void setCreatorID(int creatorID) {
		this.creatorID = creatorID;
	}

	public String getTourName() {
		return tourName;
	}

	public void setTourName(String tourName) {
		this.tourName = tourName;
	}

	public String getTourAbout() {
		return tourAbout;
	}

	public void setTourAbout(String tourAbout) {
		this.tourAbout = tourAbout;
	}

	public Coordinate getStartCoordinate() {
		return startCoordinate;
	}

	public void setStartCoordinate(Coordinate startCoordinate) {
		this.startCoordinate = startCoordinate;
	}
	
	public boolean getHasMedia() {
		return hasMedia;
	}

	public void setHasMedia(boolean hasMedia) {
		this.hasMedia = hasMedia;
	}

	public String toString()
	{
		return this.getTourID() + "\n" +
			   this.getTourName() + "\n" +
			   this.getCreatorID() + "\n" +
			   this.getTourAbout() + "\n" +
			   this.getStartCoordinate().getLatitude() +"\t" + this.getStartCoordinate().getLongitude();
			   
				
		
	}
}
