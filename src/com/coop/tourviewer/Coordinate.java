package com.coop.tourviewer;

import java.io.Serializable;


public class Coordinate implements Serializable
{
	  // Expressed in seconds of degree, positive values for north
    private double latitude;
    
	// Expressed in seconds of degree, positive values for east
    private double longitude;


    public Coordinate()
    {
        latitude = longitude = 0.0f;
    }
    
    public Coordinate(double lat, double lon)
    {
        latitude = lat;
        longitude = lon;
    }
    
    public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
    
    
    
    

}
